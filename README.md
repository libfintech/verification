# 短信验证码和通知

### 简介 

本项目集成多种短信供应商SDK，实现了短信验证码的业务逻辑，简化短信验证码接入成本。

 **当前提供的功能如下：** 

1、短信验证码过期机制

2、短信验证码发送次数限制

3、短信验证码发送频繁后触发图形验证码验证机制

4、短信验证码重试时间间隔限制

5、根据业务标志隔离短信验证码作用域

6、短信通知发送机制

图形验证码部分基于patchca（github的开源项目，地址：https://github.com/bingoohuang/patchca ）实现。

本项目持久化机制，默认为基于caffeine的本地仓储，可指定基于redis的远程仓储。

 **当前集成的短信供应商SDK如下：** 

1、云片网（https://www.yunpian.com/）
2、助通科技（http://www.ztinfo.cn/）


### 使用指南 

1、环境：jdk1.8及以上

2、入口为VerificationUtil工具类和ISmsService接口

- 创建短信服务：VerificationUtil.createSmsService(BaseConfig config);

- 发送验证码：ISmsService.send(String phoneNo, String code, String text, String flag);

- 发送通知：void send(String phoneNo, String text);

- 验证验证码：ISmsService.verify(String phoneNo, String smsCode, String imageCode, String flag);

- 刷新图片验证码：ISmsService.refreshImageCode(String phoneNo, String flag);

3、用法：[VerificationTesting](https://gitee.com/libfintech/verification/blob/master/src/test/java/testing/VerificationTesting.java)

4、扩展其他运营商sdk或api

- 配置：需实现BaseConfig抽象类

- 服务：需实现BaseSmsService抽象类

详细的参考com.libfintech.verification.service.impl中的[YunpianConfig](https://gitee.com/libfintech/verification/blob/master/src/main/java/com/libfintech/verification/service/impl/YunpianConfig.java)和[YunpianSmsService](https://gitee.com/libfintech/verification/blob/master/src/main/java/com/libfintech/verification/service/impl/YunpianSmsService.java)


### MAVEN引用

```xml
<dependency>
    <groupId>com.libfintech</groupId>
    <artifactId>verification</artifactId>
    <version>2.3</version>
</dependency>
``` 


