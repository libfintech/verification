package testing;

import com.github.bingoohuang.patchca.service.Captcha;
import com.libfintech.verification.util.CaptchaUtil;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by liamjung on 2018/1/19.
 */
public class CaptchaTesting {

    /**
     * 生成图片验证码
     */
    @Test
    public void test() {

        Captcha captcha = CaptchaUtil.create();

        BufferedImage image = captcha.getImage();
        String base64 = null;

        ByteArrayOutputStream bos = null;

        try {
            bos = new ByteArrayOutputStream();

            ImageIO.write(image, "png", bos);
            byte[] imageBytes = bos.toByteArray();

            base64 = org.apache.commons.codec.binary.Base64.encodeBase64String(imageBytes);
        } catch (IOException ioe) {
            //打印异常，并忽略
            ioe.printStackTrace();
        } finally {

            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException ioe) {
                    //打印异常，并忽略
                    ioe.printStackTrace();
                }
            }
        }

        base64 = "data:image/png;base64," + base64;

        System.out.println(base64);
    }
}
