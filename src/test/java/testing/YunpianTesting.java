package testing;

import com.libfintech.verification.service.ISmsService;
import com.libfintech.verification.service.Result;
import com.libfintech.verification.service.impl.YunpianConfig;
import com.libfintech.verification.util.VerificationUtil;
import org.junit.Test;

/**
 * Created by liamjung on 2018/1/19.
 */
public class YunpianTesting {

    private static final ISmsService SMS_SERVICE;

    static {
        YunpianConfig config = new YunpianConfig();
        //云片提供的key
        config.setApiKey("");
        //不设置仓储配置表示本地仓储（缓存）
//        config.setRepositoryConfig(null);


//        //redis仓储配置
//        RedisRepositoryConfig repositoryConfig = new RedisRepositoryConfig();
//        //redis所在主机ip和端口
//        repositoryConfig.setHost("");
//        //redis密码，若无密码时，不需要配置
//        repositoryConfig.setPass("");
//        //redis数据库，不建议修改默认值
//        repositoryConfig.setDb(2);
//        config.setRepositoryConfig(repositoryConfig);


        SMS_SERVICE = VerificationUtil.createSmsService(config);
    }

    /**
     * 测试发送验证码
     */
    @Test
    public void testSending() {

        //测试手机号
        String phoneNo = "";
        //生成的短信验证码
        String code = "";
        //短信内容
        String text = "";
        //图片验证码
        String imageCode = "";
        //业务标志
        String flag = "";

        Result<Void> result = SMS_SERVICE.send(phoneNo, code, text, imageCode, flag);

        System.out.println(result.getErrorMsg());
    }

    /**
     * 测试验证验证码
     */
    @Test
    public void testVerifying() {

        //测试手机号
        String phoneNo = "";
        //短信验证码
        String smsCode = "";
        //业务标志
        String flag = "";

        Result<Void> result = SMS_SERVICE.verify(phoneNo, smsCode, flag);

        System.out.println(result.getErrorMsg());
        System.out.println(result.getSuccess());
    }

    /**
     * 刷新图片验证码
     */
    @Test
    public void testRefreshingImageCode() {

        //测试手机号
        String phoneNo = "";
        //业务标志
        String flag = "";

        Result<String> result = SMS_SERVICE.refreshImageCode(phoneNo, flag);

        System.out.println(result.getErrorMsg());
        System.out.println(result.getData());
    }
}
