package com.libfintech.verification.repository.config.impl;

import com.libfintech.verification.repository.config.IRepositoryConfig;
import lombok.Getter;
import lombok.Setter;

/**
 * redis仓储配置实现类
 * Created by liamjung on 2018/1/23.
 */
@Getter
@Setter
public class RedisRepositoryConfig implements IRepositoryConfig {

    /**
     * redis所在主机ip和端口
     * 格式：ip:port
     * 默认值：127.0.0.1:6379
     */
    private String host = "127.0.0.1:6379";

    /**
     * redis密码，若无密码时，不需要配置
     * 默认值：null
     */
    private String pass = null;

    /**
     * redis数据库，不建议修改默认值
     * 默认值：2
     */
    private int db = 2;
}
