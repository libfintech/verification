package com.libfintech.verification.util;

import com.libfintech.verification.service.BaseConfig;
import com.libfintech.verification.service.ISmsService;
import com.libfintech.verification.service.impl.YunpianConfig;
import com.libfintech.verification.service.impl.YunpianSmsService;
import com.libfintech.verification.service.impl.ZhutongConfig;
import com.libfintech.verification.service.impl.ZhutongSmsService;

/**
 * 验证码工具类
 * Created by liamjung on 2018/1/22.
 */
public class VerificationUtil {

    /**
     * 创建云片短信服务
     *
     * @return
     */
    public static ISmsService createSmsService(BaseConfig config) {

        if (config instanceof YunpianConfig)
            return new YunpianSmsService((YunpianConfig) config);
        else if (config instanceof ZhutongConfig)
            return new ZhutongSmsService((ZhutongConfig) config);

        //可扩展为其他短信运营上

        return null;
    }
}
