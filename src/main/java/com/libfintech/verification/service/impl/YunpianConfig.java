package com.libfintech.verification.service.impl;

import com.libfintech.verification.service.BaseConfig;
import lombok.Getter;
import lombok.Setter;

/**
 * 云片配置实现类
 * Created by liamjung on 2018/1/22.
 */
@Getter
@Setter
public class YunpianConfig extends BaseConfig {

    /**
     * 云片提供的key
     */
    private String apiKey;
}
