package com.libfintech.verification.service.impl;

import com.libfintech.verification.service.BaseSmsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 助通短信服务实现类
 * Created by liamjung on 2018/8/16.
 */
@Slf4j
public class ZhutongSmsService extends BaseSmsService<ZhutongConfig> {

    private static final String URL = "http://api.zthysms.com/sendSmsBatch.do";

    private final String USERNAME;
    private final String PASSWORD;

    private final RestTemplate REST_TEMPLATE;

    public ZhutongSmsService(ZhutongConfig config) {
        super(config);

        USERNAME = config.getUsername();
        PASSWORD = config.getPassword();

        REST_TEMPLATE = new RestTemplate();
        //解决中文乱码
        //RestTemplate默认构造方法中StringHttpMessageConverter在list中的索引是1
        REST_TEMPLATE.getMessageConverters().set(1, new StringHttpMessageConverter(StandardCharsets.UTF_8));
    }

    @Override
    public void send(String phoneNo, String text) {

        String tKey = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        String password = this.generateEncryptedPassword(tKey);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("username", USERNAME);
        body.add("tkey", tKey);
        body.add("password", password);
        body.add("mobile", phoneNo);
        body.add("content", text);

        HttpHeaders headers = new HttpHeaders();
        //解决问题：ResourceAccessException: I/O error...
        headers.set("Connection", "Close");
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(body, headers);

        ResponseEntity<String> resp = REST_TEMPLATE.exchange(URL, HttpMethod.POST, httpEntity, String.class);

        String respBody = resp.getBody();

        if (resp.getBody().startsWith("1,"))
            log.info("发送成功，{}", respBody);
        else
            log.error("发送失败，{}", respBody);
    }

    /**
     * 生成加密后密码
     *
     * @param tKey
     * @return
     */
    private String generateEncryptedPassword(String tKey) {

        String password = DigestUtils.md5Hex(PASSWORD);

        password = DigestUtils.md5Hex(password.toLowerCase() + tKey);

        return password.toLowerCase();
    }
}
