package com.libfintech.verification.service.impl;

import com.libfintech.verification.service.BaseConfig;
import lombok.Getter;
import lombok.Setter;

/**
 * 助通配置实现类
 * Created by liamjung on 2018/8/16.
 */
@Getter
@Setter
public class ZhutongConfig extends BaseConfig {

    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
}
